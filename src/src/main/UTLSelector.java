package src.main;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import com.tlc.encryption.EncryptFunction;


public class UTLSelector {
	
	EncryptFunction en = new EncryptFunction();

	public static void main(String[] args) throws IOException, InterruptedException {
	
		SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy");  
        Date date = new Date();
        String currDate = formatter.format(date);
        System.out.println("Current Date: "+currDate);
		
        String username = "MSNTBANKACCNT", password = "equity_321";
        int expectedRecords = 0;
        int ctr = 0;
        int min =  1;
        int max = 60000;
        
		System.out.println("Connecting to database, please wait.");
		
		List<List<String>> rows = new ArrayList<>();
		
		try {
			
			Class.forName("oracle.jdbc.driver.OracleDriver");
			Connection con=DriverManager.getConnection(  
					"jdbc:oracle:thin:@//MMSTBYBKDB1.utl.co.ug:1521/MMSTBYBK" // For bank v2
					+ "",username,password);

			
			System.out.println("Connected successfully! Getting Records.");
			
			String getCountStatement = "SELECT COUNT(*) FROM  ADMDBMCJ.TBLCURRENTSTOCK WHERE WALLETID = 0";
			Statement stmt=con.createStatement();  
			ResultSet rs = stmt.executeQuery(getCountStatement);
			
			while(rs.next()) {
				expectedRecords = rs.getInt(1);
			}
			
			System.out.println("Expected number of records: "+expectedRecords);
			
			Thread.sleep(2000);
			
			String getStatement = "SELECT ID, MSISDN, COALESCE(TO_NUMBER(ADMDBMCJ.DECRYPT_REPLICA(AMOUNT, 'FhTQJUjwC4BmLBYwqKm', MSISDN)), 0)/100 AS BALANCE, UPDATEDDATE, EVD, WALLETID\r\n" + 
					"FROM ADMDBMCJ.TBLCURRENTSTOCK WHERE WALLETID = 0 AND ROWNUM BETWEEN "+min+" AND "+max;
			
			stmt=con.createStatement();  
			rs = stmt.executeQuery(getStatement);
			
			while(min < expectedRecords) {
				
				while(rs.next()) {
					
					ctr++;
					
					System.out.println(ctr+" | "+rs.getString(1)+" | "+rs.getString(2)+" | "+rs.getString(3)+" | "+rs.getString(6));
					String[] ar = {rs.getString(1),rs.getString(2),rs.getString(3),rs.getString(4),rs.getString(5),rs.getString(6)};
					List<String> af = Arrays.asList(ar);
					rows.add(af);
				
					min++;
					max++;
				}
				
				stmt=con.createStatement();  
				rs = stmt.executeQuery(getStatement);
			}
			
			con.close();
			
			System.out.println("Now Exporting 'Realtime Report"+ currDate +".csv' file.... ");
			
			FileWriter csvWriter = new FileWriter("Report"+ currDate +".csv");
			csvWriter.append("ID");
			csvWriter.append(",");
			csvWriter.append("MSISDN");
			csvWriter.append(",");
			csvWriter.append("AMOUNT");
			csvWriter.append(",");
			csvWriter.append("UPDATEDDATE");
			csvWriter.append(",");
			csvWriter.append("EVD");
			csvWriter.append(",");
			csvWriter.append("WALLETID");
			csvWriter.append("\n");
			
			for (List<String> rowData : rows) {
			    csvWriter.append(String.join(",", rowData));
			    csvWriter.append("\n");
			}

			csvWriter.flush();
			csvWriter.close();
			
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}  
	}
}
